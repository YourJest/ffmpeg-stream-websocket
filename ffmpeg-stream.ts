import ffmpeg from "fluent-ffmpeg";
import { WebSocketServer, WebSocket } from "ws";
import fs from "node:fs";

const usageString = "usage: yarn start /path/to/file";

const path = process.argv[2]

if(!path){
  console.log(usageString);
  process.exit(-1);
}

const stat = fs.statSync(process.argv[2])

if(!stat.isFile){
  console.log(usageString);
}

const wsServer = new WebSocketServer({ port: 6969 });

const clients: WebSocket[] = [];

wsServer.on("connection", (clientWs) => {
  clientWs.onclose = () => {
    clients.filter(c => c === clientWs);
    console.log("client disconnected");
  };
  clients.push(clientWs);
  console.log("new user connected: " + clientWs.url);
});

const fullPath = fs.realpathSync(path);

console.log(fullPath);

const ffCommand = ffmpeg(fullPath)
  .inputOptions(["-re", "-stream_loop -1"])
  .outputOptions(["-vcodec libx264", "-f h264"])
  .on("start", (cmdLine: string) => {
    console.log("started ffmpeg with command: " + cmdLine);
  })
  .on("stderr", (e) => {
    console.log(e);
  });

const ffStream = ffCommand.pipe();
ffStream.on("data", (chunk) => {
  clients.forEach((clientWs) => {
    clientWs.send(chunk);
  });
});

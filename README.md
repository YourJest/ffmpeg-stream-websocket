## ffmpeg-stream-websocket
Small application for streaming video to WebSocket using ffmpeg
### Installation
**You need `ffmpeg` installed on your PC!**

`yarn install` to install deps

`yarn build` to build the application

### Usage
Run `yarn start /path/to/file.mp4` to start hosting the video. By default the video will be played endlessly in h264 format. You can change the ffmpeg parameters, check [fluent-ffmpeg](https://github.com/fluent-ffmpeg/node-fluent-ffmpeg) for more info.

By default app is using port *6969*, but you can change it inside the code. Video will be available at `ws://localhost:6969`. 

If everything is good you can recieve video from web browser muxing it, for example, with **[jmuxer](https://github.com/samirkumardas/jmuxer)**